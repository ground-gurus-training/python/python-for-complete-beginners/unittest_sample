import unittest

from calculator import calculate_sum


class CalculatorTest(unittest.TestCase):
    def test_calculate_sum(self):
        self.assertEqual(calculate_sum(10, 20), 30)

    def test_calculate_sum_values(self):
        self.assertRaises(TypeError, calculate_sum, True)
        self.assertRaises(TypeError, calculate_sum, "")
