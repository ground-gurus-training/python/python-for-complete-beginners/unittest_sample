def calculate_sum(num1, num2):
    if type(num1) not in [int, float]:
        raise TypeError("Num1 must be a number")

    if type(num2) not in [int, float]:
        raise TypeError("Num2 must be a number")

    return num1 + num2
